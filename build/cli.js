/*
 * @Author: hucheng
 * @Date: 2020-05-10 18:45:52
 * @Description: here is des
 */
const { execSync } = require('child_process');
const { log } = console;
const { getPageNames } = require('./utils');

const action = process.argv[2];

// eslint-disable-next-line camelcase
const fn_map = {
    serve: vueServe,
    build: vueBuild,
}
if (!fn_map[action]) {
    throw new Error('No action matched');
}
fn_map[action]();

function vueServe () {
    const chalk = require('chalk');
    const inquirer = require('inquirer');
    const fuzzy = require('fuzzy');

    inquirer.registerPrompt('autocomplete', require('inquirer-autocomplete-prompt'));

    function searchPages (answers, input) {
        input = input || '';
        return new Promise(resolve => {
            const fuzzyResult = fuzzy.filter(input, getPageNames());
            resolve(fuzzyResult.map(el => el.original));
        });
    }

    inquirer
        .prompt([
            {
                type: 'autocomplete',
                name: 'page',
                message: 'Select a page to serve',
                pageSize: 10,
                source: searchPages
            }
        ])
        .then(({ page: pageName }) => {
            log(`${chalk.blue.bold('Waiting For Server Start...')}`);
            execSync(`PAGE_NAME=${pageName} npx vue-cli-service serve`, { stdio: 'inherit' });
        });
}
function vueBuild () {
    const pageName = process.argv[3];
    const env = process.argv[4];

    if (!pageName) {
        console.error('No page specific');
        process.exit(0);
    }

    if (!['qa', 'pre', 'prd'].includes(env)) {
        console.error('Environment qa/pre/prd must specific');
        process.exit(0);
    }

    execSync(
        `rm -rf ./dist/${pageName} && PAGE_NAME=${pageName} npx vue-cli-service build --mode ${env}`,
        { stdio: 'inherit' }
    );
}
