###
 # @Author: hucheng
 # @Date: 2020-05-10 18:45:05
 # @Description: here is des
 ###
#!/bin/bash
set -e

# get env by current branch name
branch=$CI_COMMIT_REF_NAME
if [ "$branch" = "develop" ]; then
    env=qa
elif [ "$branch" = "preview" ]; then
    env=pre
elif [ "$branch" = "master" ]; then
    env=prd
else
    echo "[CI] Current branch is invaild"
fi

# diff modifed page for building
search_dir=apps
counter=0
for path in "$search_dir"/*; do
    if [ $(git diff HEAD~ --name-only | grep "$path") ]; then
        page_name=$(basename $path)
        echo "[CI] Page \"$page_name\" has been modified"
        echo "[CI] Start building"
        npm run build $page_name $env
        counter=$((counter + 1))
    fi
done

if [ "$counter" -eq "0" ]; then
    echo "[CI] No page has been modifed"
    echo "[CI] Skip Building"
fi